package neomorning.com.thedavincieggs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;

public class MainActivity extends Activity {

	private Preview mPreview;
	private DrawOnTop mDrawOnTop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Hide the window title.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Create our Preview view and set it as the content of our activity.
		// Create our DrawOnTop view.
		mDrawOnTop = new DrawOnTop(this);
		mPreview = new Preview(this, mDrawOnTop);
		setContentView(mPreview);
		addContentView(mDrawOnTop, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	}

}

class DrawOnTop extends View {
	Paint mPaintBlack;
	Paint mPaintYellow;
	Paint mPaintRed;
	Paint mPaintGreen;
	Paint mPaintBlue;
	byte[] mYUVData;
	int[] mRGBData;
	int mImageWidth, mImageHeight;
	int[] mPos;

	public DrawOnTop(Context context) {
		super(context);

		Log.d("Class name ", context.getPackageName());
		Log.d("Resource path ", context.getPackageResourcePath());

		mPaintBlack = new Paint();
		mPaintBlack.setStyle(Paint.Style.FILL);
		mPaintBlack.setColor(Color.BLACK);
		mPaintBlack.setTextSize(25);

		mPaintYellow = new Paint();
		mPaintYellow.setStyle(Paint.Style.FILL);
		mPaintYellow.setColor(Color.YELLOW);
		mPaintYellow.setTextSize(25);

		mPaintRed = new Paint();
		mPaintRed.setStyle(Paint.Style.FILL);
		mPaintRed.setColor(Color.RED);
		mPaintRed.setTextSize(25);

		mPaintGreen = new Paint();
		mPaintGreen.setStyle(Paint.Style.FILL);
		mPaintGreen.setColor(Color.GREEN);
		mPaintGreen.setTextSize(25);

		mPaintBlue = new Paint();
		mPaintBlue.setStyle(Paint.Style.FILL);
		mPaintBlue.setColor(Color.BLUE);
		mPaintBlue.setTextSize(25);

		mYUVData = null;
		mRGBData = null;

		mPos = new int[2];
		mPos[0] = 0;
		mPos[1] = 1;

	}

	@Override
	protected void onDraw(Canvas canvas) {
		int canvasWidth = canvas.getWidth();
		int canvasHeight = canvas.getHeight();

		// Convert from YUV to RGB
		decodeYUV420SP(mRGBData, mYUVData, mImageWidth, mImageHeight);
		calculateLightPosition(mRGBData, mImageWidth, mImageHeight, mPos);


		String fnm = "img_" + mPos[1] + "_" + (31-mPos[0]); //  this is image file name
		String PACKAGE_NAME = "neomorning.com.thedavincieggs";
		int imgId = getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + fnm, null, null);

		if (BuildConfig.DEBUG) {
			System.out.println("IMG NAME :: " + fnm);
			System.out.println("IMG ID :: " + imgId);
			System.out.println("PACKAGE_NAME :: " + PACKAGE_NAME);
		}

		if (imgId != 0) {
			Drawable image = getResources().getDrawable(imgId);
			image.setBounds(0, 0, canvasWidth, canvasHeight);
			image.draw(canvas);
		}

		canvas.drawText(fnm, 10-1, 30-1, mPaintYellow);

		super.onDraw(canvas);

	} // end onDraw method

	static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
		final int frameSize = width * height;

		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}

				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				if (b < 0) b = 0; else if (b > 262143) b = 262143;

				rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
			}
		}
	}

	static public void calculateLightPosition(int[] rgb, int width, int height, int[] max_brightness_pos) {

		// divide the area to 32 x 5, calculate the brightness of each tile and get the brightest one
		int tile_width = width / 32;
		int tile_height = height / 5;

		int max_brightness = 0;

		// go through image tile by tile
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 32; j++) {
				int brightness = getBrightnessByTile(rgb, width, height, tile_width, tile_height, j, i);

				if (brightness > max_brightness) {
					max_brightness = brightness;
					max_brightness_pos[0] = j;
					max_brightness_pos[1] = i;
				}
			}
		}
	}


	static public int getBrightnessByTile(int[] rgb, int width, int height, int tile_width, int tile_height, int x, int y) {
		int brightness = 0;
		int x_orig = tile_width * x;
		int y_orig = tile_height * y;

		for (int i = y_orig; i < y_orig + tile_height; i++) {
			for (int j = x_orig; j < x_orig + tile_width; j++) {
				int pix = (i * width + j);

				int pixel_r = (rgb[pix] >> 16) & 0xff;
				int pixel_g = (rgb[pix] >> 8) & 0xff;
				int pixel_b = rgb[pix] & 0xff;

				int pixel_val = (pixel_r + pixel_g + pixel_b) /3;
				brightness += pixel_val;
			}
		}

		return brightness;
	}
}

class Preview extends SurfaceView implements SurfaceHolder.Callback {
	SurfaceHolder mHolder;
	Camera mCamera;
	DrawOnTop mDrawOnTop;
	boolean mFinished;

	Preview(Context context, DrawOnTop drawOnTop) {
		super(context);

		mDrawOnTop = drawOnTop;
		mFinished = false;

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {

		int cameraCount = 0;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();
		for (int camIdx = 0; camIdx<cameraCount; camIdx++) {
			Camera.getCameraInfo(camIdx, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				try {
					mCamera = Camera.open(camIdx);

					try {
						mCamera.setPreviewDisplay(holder);

						// Preview callback used whenever new viewfinder frame is available
						mCamera.setPreviewCallback(new Camera.PreviewCallback() {
							public void onPreviewFrame(byte[] data, Camera camera)
							{
								if ( (mDrawOnTop == null) || mFinished )
									return;

								if (mDrawOnTop.mRGBData == null)
								{
									// Initialize the draw-on-top companion
									Camera.Parameters params = camera.getParameters();
									mDrawOnTop.mImageWidth = params.getPreviewSize().width;
									mDrawOnTop.mImageHeight = params.getPreviewSize().height;
									mDrawOnTop.mRGBData = new int[mDrawOnTop.mImageWidth * mDrawOnTop.mImageHeight];
									mDrawOnTop.mYUVData = new byte[data.length];
								}

								// Pass YUV data to draw-on-top companion
								System.arraycopy(data, 0, mDrawOnTop.mYUVData, 0, data.length);
								mDrawOnTop.invalidate();
							}
						});
					}
					catch (IOException exception) {
						mCamera.release();
						mCamera = null;
					}

				} catch (RuntimeException e) {
					Log.e("Your_TAG", "Camera failed to open: " + e.getLocalizedMessage());
				}
			}
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		mFinished = true;
		mCamera.setPreviewCallback(null);
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		Camera.Parameters parameters = mCamera.getParameters();
		parameters.setPreviewSize(320, 240);
		parameters.setPreviewFrameRate(20);

		mCamera.setParameters(parameters);
		mCamera.startPreview();
	}

}
